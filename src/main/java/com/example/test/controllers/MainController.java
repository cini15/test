package com.example.test.controllers;

import com.example.test.service.NginxService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController()
@RequestMapping("nginx")
public class MainController {

    private final NginxService service;

    public MainController(NginxService service) {
        this.service = service;
    }

    @GetMapping("statistic")
    public Map<String,Object> getStat(){
        return service.getStatus();
    }
}
