package com.example.test.service;

import com.example.test.utill.ParseHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import java.util.HashMap;
import java.util.Map;

@Service
public class NginxService {
    @Value("${isDebug}")
    private String isDebug;

    public NginxService() {

    }

    public Map<String,Object> getStatus(){
        String status;
        String url = buildURL();
        RestTemplate rest= new RestTemplate();
        try {
            status=rest.getForObject(url,String.class);
        }catch (RestClientException e){
            Map<String,Object> exception=new HashMap<>();
            exception.put(e.getClass().getSimpleName(),e.getMessage());
            return exception;
        }
        return parse(status);
    }

    private String buildURL(){
        StringBuilder builder=new StringBuilder();
        builder.append("http://");
        if(isDebug.equals("true")){
            builder.append("localhost");
        }else {
            builder.append("webapp");
        }
        builder.append(":80/nginx_status");
        return builder.toString();
    }

    public Map<String,Object> parse(String status){
        Map<String,Object> map;
        map = ParseHelper.parseString(status);
        return map;
    }



}
