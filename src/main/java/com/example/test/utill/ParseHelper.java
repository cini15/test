package com.example.test.utill;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ParseHelper {
    public static final String SPACE=" ";
    public static final String DOTS=":";
    public static final String PATTERN_ARG_PARAM="(\\w+ ?\\w+? ?: \\d{1,99})";
    public static final String PATTERN_TABLE_SERVER="((\\w+\\s?){4}\\s(\\d{1,99}\\s?){3})";
    public static final String PATTERN_SAM_WORD="(\\w+)";
    public static final String PATTERN_SAM_NUMBER="(\\d{1,99})";


    public static String abrodSpaces(String target){
        return target.replaceAll(SPACE,"");
    }

    public static void addValuesToMap(Map<String,Object> map,String target){
        String[] arr= Pattern.compile(DOTS).split(abrodSpaces(target));
        arr[0] = firstCharToLoverCase(arr[0]);
        if (map==null)
            map=new HashMap<>();
        map.put(arr[0].trim(),arr[1].trim());
    }

    public static void addValuesToMap(Map<String ,Object> map, String arg, Object val){
        arg = firstCharToLoverCase(arg);
        if (map==null)
            map=new HashMap<>();
        map.put(arg,val);
    }

    public static Matcher createMatcher(String pattern, String target){
        return Pattern.compile(pattern).matcher(target);
    }

    public static String firstCharToLoverCase(String target){
        String str= (target.substring(0,1)).toLowerCase();
        str=str.concat(target.substring(1));
        return str;
    }

    public static Map<String,Object> parseString(String status)  {
        Map<String,Object> mapRes=new HashMap<>();
        if (!getSameString(status))
            throw new IllegalArgumentException("I can't parse status: "+status);
        Matcher matcher=createMatcher(PATTERN_ARG_PARAM,status);
        while (matcher.find()){
            addValuesToMap(mapRes,matcher.group());
        }
        matcher =createMatcher(PATTERN_TABLE_SERVER,status);
        if (matcher.find()){
            Matcher argsMatcher= createMatcher(PATTERN_SAM_WORD,matcher.group());
            Matcher valuesMatcher= createMatcher(PATTERN_SAM_NUMBER,matcher.group());
            String nameTable="";
            if(argsMatcher.find())
            nameTable=argsMatcher.group();
            Map<String,Object> tmpMap = new HashMap<>();
            while (argsMatcher.find() && valuesMatcher.find()){
                addValuesToMap(tmpMap,
                        argsMatcher.group(),
                        valuesMatcher.group());
            }
            addValuesToMap(mapRes,nameTable,tmpMap);
        }
        return mapRes;
    }

    public static boolean getSameString(String str) {
        Matcher matcherArgs=createMatcher(PATTERN_ARG_PARAM+"|"+PATTERN_TABLE_SERVER,str);
        return matcherArgs.lookingAt();
    }
}
