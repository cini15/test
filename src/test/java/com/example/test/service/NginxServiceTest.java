package com.example.test.service;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class NginxServiceTest {


    NginxService service = new NginxService();
    @Test
    void parse(){
        final String exempl="Active connections: 2 \n" +
                "server accepts handled requests\n" +
                " 12 12 10 \n" +
                "Reading: 0 Writing: 1 Waiting: 1 \n";

        Map<String,Object> map= new HashMap<>();
        Map<String,Object> servers= new HashMap<>();
        map.put("activeconnections","2");
        map.put("waiting","1");
        map.put("writing","1");
        map.put("reading","0");
        servers.put("requests","10");
        servers.put("accepts","12");
        servers.put("handled","12");
        map.put("server",servers);

        assertEquals(map,service.parse(exempl));
    }
}