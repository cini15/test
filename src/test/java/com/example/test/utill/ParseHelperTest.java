package com.example.test.utill;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static org.junit.jupiter.api.Assertions.*;

class ParseHelperTest {

    final String exempl="Active connections: 2 \n" +
            "server accepts handled requests\n" +
            " 12 12 109 \n" +
            "Reading: 0 Writing: 1 Waiting: 1999 \n";

    final String exempl2="Active connections: 2 \n" +
            "server accepts handled requests\n" +
            " 1 1 1 \n" +
            "Reading: 0 Writing: 1 Waiting: 1999 \n";
    final String badString = "Reading = 0 Writing = 1 Waiting = 1999" +
            "server accepts handled requests :12 :12 :109";
    @Test
    void abrodSpaces() {
        String act=" abrod spaces ";
        String exp="abrodspaces";
        assertEquals(exp,ParseHelper.abrodSpaces(act));
    }

    @Test
    void addValuesToMap() {
        String actStr=" First Arg : second ";
        Map<String,Object> actMap = new HashMap<>();
        Map<String,Object> exp=new HashMap<>();
        exp.put("firstArg","second");

        ParseHelper.addValuesToMap(actMap,actStr);
        assertEquals(exp,actMap);
    }

    @Test
    @Disabled("assertEquals don't work with Patterns")
    void createMatcher() {
        String arg="a";
        String patern="a";

        Matcher exp= Pattern.compile(patern).matcher(arg);
        Matcher act= ParseHelper.createMatcher(patern,arg);
        assertEquals(act, exp);
    }


    @Test
    void firstCharToLoverCase() {
        String act="FirstToLoverCase";
        String exp="firstToLoverCase";

        assertEquals(exp,ParseHelper.firstCharToLoverCase(act));
    }
    @Test
    void parseString(){
        Map<String,Object> map= new HashMap<>();
        Map<String,Object> servers= new HashMap<>();
        map.put("activeconnections","2");
        map.put("waiting","1999");
        map.put("writing","1");
        map.put("reading","0");
        servers.put("requests","109");
        servers.put("accepts","12");
        servers.put("handled","12");
        map.put("server",servers);
        assertEquals(map,ParseHelper.parseString(exempl));
    }

    @Test
    void parseString2(){
        Map<String,Object> map= new HashMap<>();
        Map<String,Object> servers= new HashMap<>();
        map.put("activeconnections","2");
        map.put("waiting","1999");
        map.put("writing","1");
        map.put("reading","0");
        servers.put("requests","1");
        servers.put("accepts","1");
        servers.put("handled","1");
        map.put("server",servers);
        assertEquals(map,ParseHelper.parseString(exempl2));
    }

    @Test
    void parseStringException(){
        Exception exception= assertThrows(IllegalArgumentException.class,()->ParseHelper.parseString(badString));
        assertEquals("I can't parse status: "+badString,exception.getMessage());
    }

    @Test
    void getSameString(){
        assertTrue( ParseHelper.getSameString(exempl2));
    }
    @Test
    void getSameStringNoSame(){

        assertFalse( ParseHelper.getSameString(badString));
    }
}